//
//  WeatherSettings.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/28/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import Foundation



//This is a Singelton Class Contains the settings of the weather app , all View Controllers uses this object if they want to modify anything in the settings of the app
class  WeatherSettings : NSObject{
    enum TempratureMeasureSetting{
        static let errorMeasure:Int = 0
        static let c:Int = 1
        static let f:Int = 2
    }
    enum LengthMeasureSetting{
        static let errorMeasure:Int = 0
        static let m:Int = 1
        static let cm:Int = 2
    }
    
    // the objet that always used
    static let sharedInstance = WeatherSettings()
    
    
    
    //Defining the class proprties 
    var settingsValues:[String] = ["Meters","Celsius"]
    var lengthsOptions:[String] = ["Meters","Centimeter"]
    var tempOptions:[String] = ["Celsius","Fehrenhait"]
    
    //constructor will be called once 
    override init(){
        // Load The data if exists
        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if userDefaults.valueForKey("settingsValues") != nil {
            self.settingsValues = userDefaults.valueForKey("settingsValues") as! [String]
        }
    }
    
     deinit{
    // save in user defaults the options before de allocating
        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(self.settingsValues, forKey: "settingsValues")
        userDefaults.synchronize()
    }
    
    // Supplimintary FUNCTIONS
    func getCurrentLengthMeasure()->Int{
        if self.settingsValues[0] == "Meters"{
        return LengthMeasureSetting.m
        }
        else if self.settingsValues[0] == "Centimeter"{
        return LengthMeasureSetting.cm
        }
        return LengthMeasureSetting.errorMeasure
    }
    
    func getCurrentTempratureMeasure()->Int{
        if self.settingsValues[1] == "Celsius"{
            return TempratureMeasureSetting.c
        }
        else if self.settingsValues[1] == "Fehrenhait"{
            return TempratureMeasureSetting.f
        }
        return TempratureMeasureSetting.errorMeasure
    }
}