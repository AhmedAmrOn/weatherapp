//
//  TodayModel.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/5/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import Foundation
import CoreLocation


class TodayModel:NSObject {
    // This class deals with everything that is related to the Today Tab
    
    var currentForecast:CZWeatherCurrentCondition?// The Forecast of today  //updated using updateTodayForecast Selector
    internal var currentCountry:String=""
    internal var currentCity:String=""
    internal var currentLongitude:CLLocationDegrees = CLLocationDegrees()
    internal var currentLatitude:CLLocationDegrees = CLLocationDegrees()
    internal var isCityChosenByUser:Bool=false // true : if the user selected this city , false: if the user uses the automatic location that is provided via INTULocationManager
    
    
    // updates currentForecast Variable
    //returns false if error occured
        func updateTodayForecast()->(Bool){
        // TODO :: Change it to hold the previous conditions
//        currentForecast = nil
        let request = CZOpenWeatherMapRequest.newCurrentRequest()
        if (self.currentCity == "") && (self.currentCountry == ""){
            request.location = CZWeatherLocation(fromCity: currentCity , country: self.currentCountry)
        }else{
            request.location = CZWeatherLocation(fromLatitude: self.currentLatitude, longitude: self.currentLongitude)
        }
        request.sendWithCompletion { (data, error) -> Void in
            if var weather = data {
                self.currentForecast = weather.current
            }
        }
        if self.currentForecast == nil {
            return false
        }
        return true
    }
    
     func updateLocation(currentLocation:CLLocation!){
        self.currentLatitude=currentLocation.coordinate.latitude
        self.currentLongitude=currentLocation.coordinate.longitude
    }
    
}