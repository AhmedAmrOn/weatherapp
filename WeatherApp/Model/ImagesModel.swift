//
//  ImagesModel.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/24/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import Foundation

class ImagesModel : NSObject {

    
    //MARK: -FUNCITONS
//    static func addWeatherStringWithImageString(weatherString:String , imageString : String){
//        ImagesModel.dict[weatherString] = imageString
//    }
    
    static func getWeatherImageString(weatherString:String) -> String{
        let dict = ["Sunny": "Sun_Big","Windy": "Wind_Big","Lightning": "Lightning_Big","Cloudy": "Cloudy_Big","Clouds":"Cloudy_Big","Rains":"Cloudy_Big","Other": "Sun_Big"]
        var imageString:String? = dict[weatherString]
        if let uimageString = imageString {
            return uimageString
        }
        imageString = "Other"
        return dict[imageString!]!
    
    
}

}
