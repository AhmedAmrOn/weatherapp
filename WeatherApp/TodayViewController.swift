//
//  FirstViewController.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/5/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import UIKit
import CoreLocation

class TodayViewController: UIViewController,CLLocationManagerDelegate {

    
    // MARK: -Outlets
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var weatherConditionLabel: UILabel! //"Sunny,Cloudy ... etc"
    @IBOutlet weak var weatherConditionImage: UIImageView!
    @IBOutlet weak var locationEnabledImage: UIImageView!
    @IBOutlet weak var TempLabel: UILabel! // in Celsios or Fehrinhait
    @IBOutlet weak var cityCountryLabel: UILabel! // In the Form of City,Country
    @IBOutlet weak var humidityLabel: UILabel! // humidity percentage
    @IBOutlet weak var pressurePercentageLabel: UILabel!
    @IBOutlet weak var precipitationLevelLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    
//    var optionsArr:[String]?
    
    //MARK: - VARIABLES
    
    var isLocationAutomatic:Bool=true //
    var locationManager:CLLocationManager?
    var currentForecast:CZWeatherCurrentCondition?// The Forecast of today
    var currentPlace:GMSPlace? // this is the current place if the place was chosen by searching , and it contains the lattidute and longtiude of the location and other data can help us to determine quickly the weather of this location
    var weatherSettings:WeatherSettings = WeatherSettings.sharedInstance
    //MARK: -LIFE CYCLE FUNCTIONS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //initilizing the Tab bar
        TodayViewController.initilizeUITabBarAppearance(self.tabBarController!.tabBar)
        //Set the navigation bar's title Font and text
        let titleFont = UIFont(name:"ProximaNova-Semibold" , size:18)
        let attributes = [NSFontAttributeName : titleFont!, NSForegroundColorAttributeName : UIColor.darkGrayColor()]
        self.navigationBar.titleTextAttributes = attributes
        //Initliaize the location manager and sets its delegate to this class
//        self.locationManager = CLLocationManager()
        self.locationManager!.delegate = self
        // Request the location services just when the application is open 

        // this line only works if the location services is not yet enabled
        self.locationManager!.requestWhenInUseAuthorization()
        // inililaize labels
        self.initLabels()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        if let location = self.locationManager!.location {
            if self.isLocationAutomatic == true {
            self.setCountryAndCity(location)
            self.updateTodayForecast()
            }
        }
        self.locationEnabledImage.hidden = !isLocationAutomatic
        // check if there is a specified Choosed location
        if(!isLocationAutomatic){
            if let place = self.currentPlace {
                var addressArr = place.formattedAddress.componentsSeparatedByString(",")
                self.cityCountryLabel.text = "\(place.name),\(addressArr[addressArr.count-1])"
                updateTodayForecast(place.coordinate.longitude,latitude:place.coordinate.latitude)
                
            }
        }else{
            // Automatic location
            locationManager!.awakeFromNib()
            if CLLocationManager.locationServicesEnabled() {
                locationManager!.startUpdatingLocation()
                if(locationManager!.location != nil){
                    setCountryAndCity(locationManager!.location)
                    updateTodayForecast()
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -IBACTIONS
    
    
    @IBAction func shareButtonPressed(sender: UIButton) {
        let textToShare = "The Weather in \(cityCountryLabel.text!) is \(weatherConditionLabel.text!) with a temprature of \(TempLabel.text!)"
        var activityVC:UIActivityViewController = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        //IMPROVMENT : could be improved to exclude other activties 
        self.parentViewController!.presentViewController(activityVC, animated: true, completion: nil)
        
    }
    
    @IBAction func showLocationsPressed(sender: UIBarButtonItem) {
        var locationVC:LocationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("locationView") as! LocationViewController
        
        locationVC.addNewData(self.weatherConditionLabel.text!, city: self.cityCountryLabel.text!.componentsSeparatedByString(",")[0], temp: self.TempLabel.text!.componentsSeparatedByString(".")[0], isAuto: self.isLocationAutomatic)
        self.presentViewController(locationVC, animated: true, completion: nil)
        
        
    }
    
    
    //MARK: -SUPPORTING FUNCTIONS
    
    
    func initLabels()
    {
        self.TempLabel.text = "--"
        self.weatherConditionLabel.text = "--"
        self.humidityLabel.text = "--"
        self.pressurePercentageLabel.text = "--"
        self.precipitationLevelLabel.text = "--"
        self.windDirectionLabel.text = "--"
        self.windSpeedLabel.text = "--"
    }
    
    func setCountryAndCity(location:CLLocation){
        // stop updating for battery life
//        locationManager.stopUpdatingLocation()
        let geocoder:CLGeocoder = CLGeocoder()
        var placeMark:CLPlacemark?
        geocoder.reverseGeocodeLocation(locationManager!.location, completionHandler: {( placemarks,error)->Void in
            if placemarks.count > 0 {
                placeMark = placemarks[0] as? CLPlacemark
                self.cityCountryLabel.text = "\(placeMark!.locality),\(placeMark!.country)"
            }
        })
    }
    

    
    
    func setWeatherDataInLabels(var weatherData:CZWeatherCurrentCondition){
        // TODO : Change the picture based on summary
        self.weatherConditionLabel.text = weatherData.summary
        // TODO Switch between the high and the low temp according to the time
        if weatherSettings.getCurrentTempratureMeasure() == WeatherSettings.TempratureMeasureSetting.c{
        self.TempLabel.text=String(format: "%.1f", weatherData.temperature.c)+"℃"
        } else {
            self.TempLabel.text=String(format: "%.1f", weatherData.temperature.f)+"℉"
        }
        self.humidityLabel.text = String(format: "%.2f", weatherData.humidity)+" mm"
        
        self.pressurePercentageLabel.text = String(format: "%.2f",(weatherData.pressure.inch))+" hPa"
        
        self.precipitationLevelLabel.text = "0.0"
        self.windSpeedLabel.text = String(format: "%.2f",(weatherData.windSpeed.mph))+" mph"
        self.windDirectionLabel.text = String(format: "%.2f",(weatherData.windDirection.description))
        self.weatherConditionImage.image = UIImage(named:            ImagesModel.getWeatherImageString(weatherData.summary))

    }
    
    
    func updateTodayForecast(longitude:CLLocationDegrees ,latitude:CLLocationDegrees)->Bool{
        let request = CZOpenWeatherMapRequest.newCurrentRequest()
        request.location = CZWeatherLocation(fromLatitude: latitude, longitude: longitude)
        request.sendWithCompletion { (data, error) -> Void in
            if var weather = data {
                //                self.currentForecast = weather.current
                self.setWeatherDataInLabels(weather.current)
            }
        }
        if self.currentForecast == nil {
            return false
        }
        return true
    }
    
    
    
    func updateTodayForecast()->(Bool){
        // TODO :: Change it to hold the previous conditions
        let request = CZOpenWeatherMapRequest.newCurrentRequest()
        request.location = CZWeatherLocation(fromLatitude: self.locationManager!.location.coordinate.latitude,longitude: self.locationManager!.location.coordinate.longitude)
        request.sendWithCompletion { (data, error) -> Void in
            if var weather = data {
                self.setWeatherDataInLabels(weather.current)
            }
        }
        if self.currentForecast == nil {
            return false
        }
        return true
    }
    
    
    func setFonts(){
        let titleFont = UIFont(name:"Proxima Nova" , size:18)
        let attributes = [NSFontAttributeName : titleFont!, NSForegroundColorAttributeName : UIColor()]
        self.navigationController!.navigationBar.titleTextAttributes = attributes
    }
    
    static func initilizeUITabBarAppearance(tabbar:UITabBar){
        //This method has the code to initilize the Tab Bar with the required appearance and buttons
        
        let selectedtodayImg = UIImage(named:"TodaySelected.png")
        let unselectedtodayImg = UIImage(named:"TodayUnselected.png")
        let selectedForcastImg = UIImage(named:"ForecastSelected")
        let unselectedForcastImg = UIImage(named:"ForecastUnselected")
        let selectedSettingsImg = UIImage(named: "SettingsSelected")
        let unselectedSettingsImg = UIImage(named: "SettingsUnselected")
        
        let todayItem:UITabBarItem = (tabbar.items! as NSArray).objectAtIndex(0) as! UITabBarItem
        let forcastItem:UITabBarItem = (tabbar.items! as NSArray).objectAtIndex(1) as! UITabBarItem
        let settingsItem:UITabBarItem = (tabbar.items! as NSArray).objectAtIndex(2) as! UITabBarItem
        

        
        todayItem.selectedImage = selectedtodayImg
        todayItem.image = unselectedtodayImg
        todayItem.title = "Today"
        forcastItem.selectedImage = selectedForcastImg
        forcastItem.image = unselectedForcastImg
        forcastItem.title = "Forecast"
        settingsItem.selectedImage = selectedSettingsImg
        settingsItem.image = unselectedSettingsImg
        settingsItem.title = "Settings"
        
        // Setting the background Image with the supplied bar image
        UITabBar.appearance().backgroundImage = UIImage(named: "Bar.png")
        UITabBar.appearance().backgroundColor = UIColor.blackColor()
    }


}