//
//  weatherCellTypeTableViewCell.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/17/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import UIKit

class weatherCellTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var isAutoLocationLabel: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    

}

