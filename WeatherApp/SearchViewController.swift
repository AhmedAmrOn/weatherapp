//
//  SearchViewController.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/19/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import UIKit


class SearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate {

    // MARK -- OUTLETS
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK -- VARIABLES
    var data = [GMSAutocompletePrediction]()
    var finalPlace:GMSPlace?
    var arr = [GMSPlace]()

    //MARK -- VIEW FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.setImage(UIImage(named:"Search.png"), forSearchBarIcon: UISearchBarIcon.Search, state: UIControlState.Normal)
        searchBar.setImage(UIImage(named: "Close.png"), forSearchBarIcon: UISearchBarIcon.Clear, state: UIControlState.Normal)
        searchBar.setSearchFieldBackgroundImage(UIImage(named:"Input-1.png"), forState: UIControlState.Normal)
        searchBar.backgroundColor = UIColor.whiteColor()
        searchBar.searchTextPositionAdjustment = UIOffset(horizontal: CGFloat(2), vertical: CGFloat(0))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK -- TABLE VIEW FUNCTIONS
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let tableCell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("searchResultCell") as! UITableViewCell
        var item:GMSAutocompletePrediction =  self.data[indexPath.row]
        // Applying text bolding
        var normalFont = UIFont(name:"ProximaNova-Regular" , size:16)
        var boldFont = UIFont(name:"ProximaNova-Semibold" , size:16)
        
        var bolded:NSMutableAttributedString = item.attributedFullText.mutableCopy() as! NSMutableAttributedString
        
        
        bolded.enumerateAttribute(kGMSAutocompleteMatchAttribute, inRange: (NSMakeRange(0, bolded.length)), options: NSAttributedStringEnumerationOptions.allZeros, usingBlock: {
            (value:AnyObject?,range:NSRange,stop:UnsafeMutablePointer<ObjCBool>)->Void in
            var choosedFont = UIFont()
            if (value != nil){
                choosedFont = boldFont!
                
            }else{
                choosedFont = normalFont!
            }
            
            bolded.addAttribute(NSFontAttributeName, value: choosedFont, range: range)
        })
        
        tableCell.textLabel?.attributedText = bolded
        return tableCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // City Selected 
        // Request More Details about the city
        // getting the coordinaties to pass it with the city and country name
        
        var placesClient:GMSPlacesClient = GMSPlacesClient()
        var finalPlace:GMSPlace?
        var selectedCity:GMSAutocompletePrediction = self.data[indexPath.row]
        var locationsView:LocationViewController = self.presentingViewController as! LocationViewController
        let place:String = selectedCity.placeID
        placesClient.lookUpPlaceID(place, callback: {
            (place,error) -> Void  in
            if error != nil {
                println("lookup place id query error: \(error!.localizedDescription)")
                return
            }
            if let uplace:GMSPlace = place  {
                locationsView.placesArr.append(uplace)
                locationsView.placesIDs.append(uplace.placeID)
                locationsView.client = placesClient as GMSPlacesClient
                locationsView.saveData()
                locationsView.createForecastOf(uplace)
                // go back to the Locations
                self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
            }else{
                println("No Place Details for this city")
            }
            
        })
    }
        
    
    // MARK: -Search
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchCitiesForSearchText(self.searchBar.text, scope: "CitySearch")

    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchCitiesForSearchText(self.searchBar.text, scope: "CitySearch")
    }
    
    func searchCitiesForSearchText(searchText: String , scope: String = "CitySearch"){
        // MARK: Testing Google API
        var placesClient:GMSPlacesClient = GMSPlacesClient()
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.City
        if count(searchText) > 0 {
            println("Searching for '\(searchText)'")
            placesClient.autocompleteQuery(searchText, bounds: nil, filter: filter, callback: { (results, error) -> Void in
                if error != nil {
                    println("Autocomplete error \(error) for query '\(searchText)'")
                    return
                }
                
                println("Populating results for query '\(searchText)'")
                self.data = [GMSAutocompletePrediction]()
                if let uResults = results {
                    for result in uResults {
                        if let result:GMSAutocompletePrediction = result as? GMSAutocompletePrediction {
                            self.data.append(result)
                        }
                    }
                }
                self.tableView.reloadData()
                for item in self.data  {
                    println(item)
                }
            })
        } else {
            self.data = [GMSAutocompletePrediction]()
            self.tableView.reloadData()
        }
    }
    
    
    //MARK: -ACTIONS

    @IBAction func cancelAction(sender: UIButton) {
         self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
}

