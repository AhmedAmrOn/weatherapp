//
//  SecondViewController.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/5/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {

    
    //MARK: -OUTLETS
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var locationButton: UIBarButtonItem!
    
    //MARK: -VARIABLES
    var cityName:String="Forecast"
    var weatherArr:[CZWeatherForecastCondition]=[]
    var numOfForecastDays:Int = 4
    var currentPlace:GMSPlace?
    var currentAutoLocation:CLLocation?
    var weatherSettings:WeatherSettings = WeatherSettings.sharedInstance
    
    
    // MARK: VIEW CYCLE FUCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        var nib = UINib(nibName: "weatherCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "weatherCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //Set the navigation bar's title Font and text
        let titleFont = UIFont(name:"ProximaNova-Semibold" , size:18)
        let attributes = [NSFontAttributeName : titleFont!, NSForegroundColorAttributeName : UIColor.darkGrayColor()]
        self.navigationBar.titleTextAttributes = attributes
        
//        var rootVC:UITabBarController = self.presentingViewController
//        
        
        

        // if the user uses the automatic location the current place will be nil
        


        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        var todayVC:TodayViewController = (self.tabBarController!.viewControllers!.first as! TodayViewController)
        self.currentPlace = todayVC.currentPlace
        self.currentAutoLocation = todayVC.locationManager!.location
        //settig the city name as title to the navigaion bar
        //check if there is a value in current place
        if let ucurrentPlace = self.currentPlace {
            self.cityName = ucurrentPlace.name
            self.updateWeatherForecastOfPlace(self.currentPlace!)
        }else if let ucurrentAutoLocation = self.currentAutoLocation{
            self.cityName = todayVC.cityCountryLabel.text!.componentsSeparatedByString(",")[0]
            self.updateWeatherForecastOfAutoLocation(ucurrentAutoLocation)
        }else{
            self.cityName = "Forecast"
        }
        self.navigationBar.topItem?.title = self.cityName
    }

    
    //MARK: TABLE FUCTIONS
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.weatherArr.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:weatherCellTypeTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("weatherCell") as! weatherCellTypeTableViewCell
        var weather:CZWeatherForecastCondition = self.weatherArr[indexPath.row]
        var weekday:NSDateFormatter = NSDateFormatter()
        // According to http://unicode.org/reports/tr35/tr35-6.html#Date_Format_Patterns
        weekday.dateFormat = "EEEE"
        cell.cityLabel.text = weekday.stringFromDate(weather.date)
        cell.weatherLabel.text = weather.summary
        if self.weatherSettings.getCurrentTempratureMeasure() == WeatherSettings.TempratureMeasureSetting.c{
                cell.tempLabel.text = "\(Int(weather.highTemperature.c))"
        }else{
            cell.tempLabel.text = "\(Int(weather.highTemperature.f))"
        }

        cell.weatherImage.image = UIImage(named: ImagesModel.getWeatherImageString(weather.summary))
        cell.isAutoLocationLabel.hidden = true
        return cell
    }
    
    
    //MARK: WEATHER FORECAST FUNCTIONS
    func updateWeatherForecastOfPlace(place:GMSPlace){
        let request = CZOpenWeatherMapRequest.newDailyForecastRequestForDays(self.numOfForecastDays)
        
        request.location = CZWeatherLocation(fromLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                self.weatherArr = []
        request.sendWithCompletion { (data, error) -> Void in
            if let weather = data {
                for forecast in weather.dailyForecasts {
                    self.weatherArr.append(forecast as! CZWeatherForecastCondition)
                }
                self.tableView.reloadData()
            }
    }
    }
    
    func updateWeatherForecastOfAutoLocation(autolocation:CLLocation){
        let request = CZOpenWeatherMapRequest.newDailyForecastRequestForDays(self.numOfForecastDays)
        
        request.location = CZWeatherLocation(fromLatitude: autolocation.coordinate.latitude, longitude: autolocation.coordinate.longitude)
        self.weatherArr = []
        request.sendWithCompletion { (data, error) -> Void in
            if let weather = data {
                for forecast in weather.dailyForecasts {
                    self.weatherArr.append(forecast as! CZWeatherForecastCondition)
                }
                self.tableView.reloadData()
            }
        }
    }
    

    
    
    
    
    

}

