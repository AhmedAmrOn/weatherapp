//
//  LocationViewController.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/17/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import UIKit

class LocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var navigationBar: UINavigationBar!


    @IBOutlet weak var overlayImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addImage: UIImageView!

    var weatherArr:[String] = []
    var cityArr:[String] = []
    var images:[String] = []
    var temp:[String] = []
    var isAutoArr:[Bool]=[]
    var placesIDs:[String] = []
    var placesArr:[GMSPlace] = [GMSPlace]()// off by one , the first item in all the above is the current city in today view
    var client:GMSPlacesClient?
    func addNewData(currentWeather:String, city:String , temp:String , isAuto:Bool){
        // add this data
        self.weatherArr.append(currentWeather)
        self.cityArr.append(city)
        self.temp.append(temp)
        self.isAutoArr.append(isAuto)
        var weatherImageName:String = ImagesModel.getWeatherImageString(currentWeather)
            self.images.append(weatherImageName)
    }
    
    func saveData(){
        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(self.weatherArr, forKey: "weatherArr")
        userDefaults.setValue(self.cityArr, forKey: "cityArr")
        userDefaults.setValue(self.images,  forKey: "images")
        userDefaults.setValue(self.temp, forKey: "temp")
        userDefaults.setValue(self.isAutoArr, forKey: "isAutoArr")
        userDefaults.setValue(self.placesIDs, forKey: "placesIDs")
//        saveGMSPlaceArr()
        userDefaults.synchronize()
        println("SAVING DATA : \n ARRAY COUNTS : \n WEATHER: \(weatherArr.count) \n CITY: \(cityArr.count) \n IMAGES: \(images.count) \n TEMP : \(temp.count) \n ISAUTO : \(isAutoArr.count) \n PLACES : \(placesArr.count)")
    }
    
    func loadData(){
        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if  userDefaults.valueForKey("weatherArr") != nil {
        self.weatherArr = userDefaults.valueForKey("weatherArr") as! [String]
        self.cityArr = userDefaults.valueForKey("cityArr") as! [String]
        self.images = userDefaults.valueForKey("images") as! [String]
        self.temp = userDefaults.valueForKey("temp") as! [String]
        self.isAutoArr = userDefaults.valueForKey("isAutoArr") as! [Bool]
        self.placesIDs = userDefaults.valueForKey("placesIDs") as! [String]
        self.loadGMSPlaceArr()
        }
        println("LOADING DATA : \n ARRAY COUNTS : \n WEATHER: \(weatherArr.count) \n CITY: \(cityArr.count) \n IMAGES: \(images.count) \n TEMP : \(temp.count) \n ISAUTO : \(isAutoArr.count) \n PLACES : \(placesArr.count)")
    }
    
    
    
    func loadGMSPlaceArr(){
        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        self.placesIDs = userDefaults.valueForKey("placesIDs") as! [String]
        self.placesArr = []
        var placesClient:GMSPlacesClient = GMSPlacesClient()
        // call to get GMSPlaces Objects
        for placeID in self.placesIDs {
            placesClient.lookUpPlaceID(placeID, callback: {
                (place, error) -> Void in
                if error != nil {
                    println("lookup place id query error: \(error!.localizedDescription)")
                    return
                }
                
                println("Place :/ " + (place! as GMSPlace ).name)
                self.placesArr.append(place! as GMSPlace)
                self.tableView.reloadData()
            }
            )}
    }
    
    
    
    
    override func viewWillDisappear(animated: Bool) {
        self.saveData()
    }
    
  
    
       override func viewDidLoad() {
        super.viewDidLoad()
        var nib = UINib(nibName: "weatherCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "weatherCell")
        
        let titleFont = UIFont(name:"ProximaNova-Semibold" , size:18)
        let attributes = [NSFontAttributeName : titleFont!, NSForegroundColorAttributeName : UIColor.darkGrayColor()]
        self.navigationBar.titleTextAttributes = attributes
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.overlayImage.userInteractionEnabled = false
        self.addImage.userInteractionEnabled = true
        //add an action
        var tab:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "addNewLocation")
        tab.numberOfTapsRequired=1
        self.addImage.addGestureRecognizer(tab)
        
        if(placesArr.count > 0){
            for place in placesArr {
                println(place.description)
            }
        }
        
//        self.tableView.editing = true
    }
    
    override func viewWillAppear(animated: Bool) {
//        NSUserDefaults.standardUserDefaults().synchronize()
        self.loadData()
        self.tableView.reloadData()
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:weatherCellTypeTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("weatherCell") as! weatherCellTypeTableViewCell
        
        cell.weatherLabel.text = self.weatherArr[indexPath.row]
        cell.cityLabel.text = self.cityArr[indexPath.row]
        var image:String = ImagesModel.getWeatherImageString(weatherArr[indexPath.row])
        cell.weatherImage.image = UIImage(named:image)
        cell.tempLabel.text = self.temp[indexPath.row]
        cell.isAutoLocationLabel.hidden = !isAutoArr[indexPath.row]
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    
    func addNewLocation(){
        var searchVC:SearchViewController = self.storyboard!.instantiateViewControllerWithIdentifier("addCityView") as! SearchViewController
        self.presentViewController(searchVC, animated: true, completion: nil)
    }
    
    @IBAction func doneAction(sender: UIBarButtonItem) {
        var barController:UITabBarController = self.presentingViewController as! UITabBarController

        var todayVC:TodayViewController = barController.viewControllers?.first! as! TodayViewController
        
        var selectedRow:NSIndexPath? =  tableView.indexPathForSelectedRow()
        
        if let selected = selectedRow {
            // we have a selection happened 
            // get the value of the place
            
            if selected.row != 0{
            var place:GMSPlace?
            var city:String = self.cityArr[selected.row] // selected city
            // we did searching instead of just getting the corosponding  index element in selectedRow from placesArr because when we load the data we send multiple asynchcrouns tasks to get the locations back into the placesArr , and it comes back not in the original order ofcourse
                for var i = 0 ; i < self.placesArr.count ; i++ {
                    if self.placesArr[i].name == city{
                        place = placesArr[i]
                        break
                    }
                }

            todayVC.currentPlace = place
            todayVC.isLocationAutomatic = false
            }else{
             todayVC.currentPlace = nil
            todayVC.isLocationAutomatic = true
            }
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createForecastOf(place:GMSPlace){
        let request = CZOpenWeatherMapRequest.newCurrentRequest()
        request.location = CZWeatherLocation(fromLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        request.sendWithCompletion { (data, error) -> Void in
            if var weather = data {
//                self.setWeatherDataInLabels(weather.current)
                var currentWeather:CZWeatherCurrentCondition = weather.current
                println("------------------------"+currentWeather.summary)
                self.addNewData(currentWeather.summary, city: place.name, temp:"\(currentWeather.temperature.c)".componentsSeparatedByString(".")[0] , isAuto: false)
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if indexPath.row > 0{
            return true
        }else{
            return false
        }

    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        var city:String = self.cityArr[indexPath.row]
        var index:Int?
        // we did searching instead of just getting the corosponding  index element in selectedRow from placesArr because when we load the data we send multiple asynchcrouns tasks to get the locations back into the placesArr , and it comes back not in the original order ofcourse
        for var i = 0 ; i < self.placesArr.count ; i++ {
            if self.placesArr[i].name == city{
                index = i
                break
            }
        }

        self.weatherArr.removeAtIndex(indexPath.row)
        self.cityArr.removeAtIndex(indexPath.row)
        self.temp.removeAtIndex(indexPath.row)
        self.isAutoArr.removeAtIndex(indexPath.row)
        self.placesArr.removeAtIndex(index!)
//        self.placesIDs.removeAtIndex(index!)
        self.saveData()
        self.tableView.reloadData()
    }
    
     func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle{
        return UITableViewCellEditingStyle.Delete
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        var button:UITableViewRowAction = UITableViewRowAction(style:UITableViewRowActionStyle.Default , title: "Delete\nLocation", handler: {
            (action,index)->Void in
            self.tableView.dataSource?.tableView?(self.tableView, commitEditingStyle: UITableViewCellEditingStyle.Delete, forRowAtIndexPath: indexPath)
        })

        button.backgroundColor =  UIColor.orangeColor()
        return [button]
        
        
    }

}
