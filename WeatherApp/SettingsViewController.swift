//
//  SettingsViewController.swift
//  WeatherApp
//
//  Created by Ahmed Amr Abdul-Fattah on 6/5/15.
//  Copyright (c) 2015 iOS_Projects. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource, UIPickerViewDelegate{
    
    
    //Mark: --IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var generalLabel: UILabel!
    
    //MARK: -- VARIABLES
    var settingsLabels:[String] = ["Unit of length " , "Units of temprature"]
    var weatherSettings:WeatherSettings = WeatherSettings.sharedInstance
    var currentChoosenOption:Int?
    
    //MARK: -VIEW CYCLE FUNCTIONS
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        // initilize the appearance of the navigation bar
        let titleFont = UIFont(name:"ProximaNova-Semibold" , size:18)
        let attributes = [NSFontAttributeName : titleFont!, NSForegroundColorAttributeName : UIColor.darkGrayColor()]
        self.navigationBar.titleTextAttributes = attributes
        
        // initlize General label
        let generalFont = UIFont(name: "ProximaNova-Semibold", size: 14)
        self.generalLabel.font = generalFont
        self.tableView.bounces = false
        self.pickerView.delegate = self
        self.pickerView.hidden = true
        //similar of hiding
        self.doneBarButton.title = ""
        self.doneBarButton.enabled = false
        
    }
    
    override func viewWillAppear(animated: Bool) {
//        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
//        if userDefaults.valueForKey("settingsValues") != nil {
//            // there exist custom configiration
//            // load it
//            self.settingsValues=[]
//            self.settingsValues = userDefaults.valueForKey("settingsValues") as! [String]
//        }
    }
    
    override func viewWillDisappear(animated: Bool) {
//        // writing the data to NSUserDefaults
//        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
//        userDefaults.setValue(self.settingsValues, forKey: "settingsValues")
//        userDefaults.synchronize()
    }
    
    
    //MARK : -TABLE METHODS
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("settingsCell") as! UITableViewCell
        
        let font = UIFont(name: "ProximaNova-Regular", size: 17)
        cell.textLabel?.font = font
        cell.textLabel?.text = self.settingsLabels[indexPath.row]
        cell.detailTextLabel?.font = font
        cell.detailTextLabel?.text = self.weatherSettings.settingsValues[indexPath.row]
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        displayPickerForOption(indexPath.row)
    }
    
    
    
    //MARK: -PICKERVIEW METHODS
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.weatherSettings.settingsValues.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if self.currentChoosenOption == 0 {
            
            return self.weatherSettings.lengthsOptions[row]
            
        }else if self.currentChoosenOption == 1 {
            return self.weatherSettings.tempOptions[row]
        }
        return ""
    }
    
    //MARK: -SUPPLIMENTARY FUCTIONS
    
    func displayPickerForOption(option:Int){
        //set the global variable with option ID
        self.currentChoosenOption = option
        // Show the Done Button
        self.doneBarButton.enabled=true
        self.doneBarButton.title = "Done"
        //
        self.pickerView.reloadComponent(0)
        self.pickerView.hidden = false
    }
    
    func hidePickerView(){
        self.doneBarButton.enabled = false
        self.doneBarButton.title = ""
        self.pickerView.hidden = true
    }
    
    
    //MARK: -- Actions
    
    @IBAction func doneAction(sender: AnyObject) {
        var selectedIndex:Int = self.pickerView.selectedRowInComponent(0)
        if currentChoosenOption == 0{
             self.weatherSettings.settingsValues[0] = self.weatherSettings.lengthsOptions[selectedIndex]
        }else if currentChoosenOption == 1 {
            self.weatherSettings.settingsValues[1] = self.weatherSettings.tempOptions[selectedIndex]
        }
        hidePickerView()
        self.tableView.reloadData()
    }
    
    
    
    
    
    
    
    
}