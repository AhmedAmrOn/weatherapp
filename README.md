# Weather App v. 1.0 # 

This is a test project in iOS for STRV Company.

### Quick summary ###
This is a Weather Application in iOS Platform with swift , It gets the weather automatically that corresponds to your current location , also it has the functionality of choosing another location , has autocomplete feature of cities , also get the autocompleted locations in your place language .

### Version ###
The Current Version is 1.0

### iOS Version ###
iOS 8.3

### Current Supported Devices ###
iPhone 5s

### Known Current Bugs in version 1.0 ###
* In the First Lunch of the App it freezes till the location services is enabled.
* Fast transitions between tabs could lead to duplication in the forecast Tab.
* Location Deletion sometimes leads to crash.

### How to run the App for the first time ###
Until the bug is solved in the next version , In the first launch of the app , click the home button then Accept the location services then go back to the app.

### Next Version 1.1 Milestone ###
- Solve the App Freezing
- Organise Code


### Later Versions Milestones ###
- Solve All Known and Incoming Bugs
- Supporting for all iOS Devices 
- Increasing the App Stability 

##------------------------------##
# Screenshots of the App v. 1.0 #
##------------------------------##
![iOS Simulator Screen Shot Jun 29, 2015, 4.54.54 PM.png](https://bitbucket.org/repo/rzgxMp/images/3162577318-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.54.54%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 4.55.25 PM.png](https://bitbucket.org/repo/rzgxMp/images/2336517569-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.55.25%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 4.56.20 PM.png](https://bitbucket.org/repo/rzgxMp/images/3876350865-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.56.20%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 4.56.54 PM.png](https://bitbucket.org/repo/rzgxMp/images/1758172179-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.56.54%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 4.57.12 PM.png](https://bitbucket.org/repo/rzgxMp/images/2396601078-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.57.12%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 4.57.18 PM.png](https://bitbucket.org/repo/rzgxMp/images/1371744963-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.57.18%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 4.57.29 PM.png](https://bitbucket.org/repo/rzgxMp/images/909098196-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.57.29%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 4.57.34 PM.png](https://bitbucket.org/repo/rzgxMp/images/1102540281-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.57.34%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 4.57.50 PM.png](https://bitbucket.org/repo/rzgxMp/images/499330730-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%204.57.50%20PM.png)

##------------------------------##
# Region Customised Results #
##------------------------------##
![iOS Simulator Screen Shot Jun 29, 2015, 5.01.35 PM.png](https://bitbucket.org/repo/rzgxMp/images/1125617519-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%205.01.35%20PM.png) 

![iOS Simulator Screen Shot Jun 29, 2015, 5.04.06 PM.png](https://bitbucket.org/repo/rzgxMp/images/2412118982-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%205.04.06%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 5.05.51 PM.png](https://bitbucket.org/repo/rzgxMp/images/3156478682-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%205.05.51%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 5.05.33 PM.png](https://bitbucket.org/repo/rzgxMp/images/1856736689-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%205.05.33%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 5.05.55 PM.png](https://bitbucket.org/repo/rzgxMp/images/401684018-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%205.05.55%20PM.png)

![iOS Simulator Screen Shot Jun 29, 2015, 5.06.00 PM.png](https://bitbucket.org/repo/rzgxMp/images/2375650095-iOS%20Simulator%20Screen%20Shot%20Jun%2029,%202015,%205.06.00%20PM.png)

### Contact for other bugs ###
Kindly if you found any other bugs in this version send me an email on 
BugsSolved@gmail.com with the bug description.