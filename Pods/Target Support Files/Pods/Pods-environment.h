
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CZWeatherKit
#define COCOAPODS_POD_AVAILABLE_CZWeatherKit
#define COCOAPODS_VERSION_MAJOR_CZWeatherKit 2
#define COCOAPODS_VERSION_MINOR_CZWeatherKit 0
#define COCOAPODS_VERSION_PATCH_CZWeatherKit 1

// GoogleMaps
#define COCOAPODS_POD_AVAILABLE_GoogleMaps
#define COCOAPODS_VERSION_MAJOR_GoogleMaps 1
#define COCOAPODS_VERSION_MINOR_GoogleMaps 10
#define COCOAPODS_VERSION_PATCH_GoogleMaps 0

